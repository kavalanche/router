# Kavalanche/Router
Simple routing library for web applications.

# Usage

1. Require kavalanche/router
    
    ```bash
    composer require kavalanche/router
    ```

2. Create `Router` instance.

    ```php
    $router = new \Kavalanche\Router\Router();
    ```

3. Add your routes.

    ```php
    $router->get('/', function() {
        echo 'Start';
    });

    $router->get('/post(\d+)', function($id) {
        echo 'Post ' . $id;
    });

    $router->get('/test/index', [Kavalanche\Router\TestController::class, 'index']);

    $controller = new Kavalanche\Router\TestController();

    $router->get('/test/post/(\d+)', [$controller, 'post']);
    ```

4. Invoke `dispatch` method.

    ```php
    $router->dispatch();
    ```