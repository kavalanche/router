<?php

declare(strict_types=1);

namespace Kavalanche\Router;

/**
 * @author wojtek
 */
class Route {

    public string $method;
    public string $path;
    public $callback;

}
