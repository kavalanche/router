<?php

declare(strict_types=1);

namespace Kavalanche\Router;

/**
 * @author wojtek
 */
class RouteNotFoundException extends \Exception {
    
}
