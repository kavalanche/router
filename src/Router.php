<?php

declare(strict_types=1);

namespace Kavalanche\Router;

/**
 * @author wojtek
 */
class Router {

    private array $allowedMethods = [
        'get',
        'post'
    ];
    private array $routes = [];

    public function request(string $method, string $path, callable $callback) {
        $route = new Route();
        $route->method = $method;
        $route->path = $path;
        $route->callback = $callback;
        $this->routes[] = $route;
    }

    public function get(string $path, callable $callback) {
        return $this->request('get', $path, $callback);
    }

    public function post(string $path, callable $callback) {
        return $this->request('post', $path, $callback);
    }

    public function dispatch() {
        $requestMethod = strtolower($_SERVER['REQUEST_METHOD']);
        $requestUri = parse_url($_SERVER['REQUEST_URI']);
        foreach ($this->routes as $route) {
            $path = str_replace('/', '\/', $route->path);
            $path = '/^' . $path . '\/?$/';
            if (in_array($requestMethod, $this->allowedMethods, true) && $requestMethod === $route->method && preg_match($path, $requestUri['path'], $params) === 1) {
                array_shift($params);
                call_user_func_array($route->callback, $params);
                exit;
            }
        }
        http_response_code(404);
        throw new RouteNotFoundException('Route not found!');
    }

    public function isRequestInRoutes(): bool {
        $requestMethod = strtolower($_SERVER['REQUEST_METHOD']);
        $requestUri = parse_url($_SERVER['REQUEST_URI']);
        foreach ($this->routes as $route) {
            $path = str_replace('/', '\/', $route->path);
            $path = '/^' . $path . '\/?$/';
            if (in_array($requestMethod, $this->allowedMethods, true) && $requestMethod === $route->method && preg_match($path, $requestUri['path'], $params) === 1) {
                return true;
            }
        }
        return false;
    }
}
